package com.continuum.activity;

import android.app.ProgressDialog;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;

import com.continuum.util.Constants;
import com.continuum.util.TransParentProgressDialog;
import com.continuum.util.Utils;
import com.continuum.util.WindowUtil;
import com.continuum.view.VideoControllerView;

import java.util.HashMap;

/**
 * Created by waqas.memon on 2/14/2016.
 */
public class VideoPlayerActivity extends BaseActivity implements SurfaceHolder.Callback, VideoControllerView.MediaPlayerControl {

    SurfaceView videoView;
    MediaPlayer player;
    VideoControllerView controller;
    private ProgressDialog prepareDialog;
    String videoURL = null;
    String videoTitle = null;
    public static ActionBar actionBar = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prepareDialog = new ProgressDialog(this, R.style.ProgreessTheme);
        prepareDialog.setCancelable(false);
        prepareDialog.setMessage("Loading...");
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        Log.i("VideoPlayer", "Player onCreate");

        setContentView(R.layout.activity_video_player);


        actionBar =  getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.hide();

        Point screenUsableSize = WindowUtil.getScreenDimensions(this);
        View videoPlayerLayout = findViewById(R.id.video_container);
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            WindowUtil.setLayoutWidth(videoPlayerLayout, screenUsableSize.x);
        }else{
            WindowUtil.setLayoutWidth(videoPlayerLayout, screenUsableSize.y);
        }

        player = new MediaPlayer();
        videoView = (SurfaceView) findViewById(R.id.videoSurface);
        controller = new VideoControllerView(this);

        SurfaceHolder videoHolder = videoView.getHolder();
        videoHolder.addCallback(this);
        videoHolder.setKeepScreenOn(true);



        try {
            Bundle b = getIntent().getExtras();
            videoURL = b.getString("videoURL");
            videoTitle = b.getString("videoTitle");
            setTitle(videoTitle);

        } catch (Exception e) {
            Utils.trackMixPanelEvent(mixpanel, Constants.MIXPANEL_EVENT_ERROR, Utils.getErrorStackTraceHashMap(e));
            Utils.flushMixPanel(mixpanel);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                goBackToParentActivity();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onBackPressed() {
        try {
            goBackToParentActivity();


        }catch (Exception e){
            e.printStackTrace();
        }finally {
            this.finish();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        controller.show();
        return false;
    }


    // Implement SurfaceHolder.Callback
    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        try {
//            String videoURL = "http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4";
            controller.setMediaPlayer(this);
            controller.setAnchorView((FrameLayout) findViewById(R.id.videoSurfaceContainer));
            player.setDisplay(holder);
            player.setOnPreparedListener(onPreparedListener);



//            player.setAudioStreamType(AudioManager.STREAM_MUSIC);

            player.setDataSource(this, Uri.parse(videoURL));
            player.setLooping(false);

            prepareDialog.show();
            player.prepareAsync();

        }catch (Exception e){
            prepareDialog.dismiss();
            Utils.trackMixPanelEvent(mixpanel, Constants.MIXPANEL_EVENT_ERROR, Utils.getErrorStackTraceHashMap(e));
            Utils.flushMixPanel(mixpanel);
            e.printStackTrace();

        }



    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }


    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        videoView.destroyDrawingCache();
    }
    // End SurfaceHolder.Callback

    // Implement VideoMediaController.MediaPlayerControl
    @Override
    public boolean canPause() {
        return true;
    }

    @Override
    public boolean canSeekBackward() {
        return true;
    }

    @Override
    public boolean canSeekForward() {
        return true;
    }

    @Override
    public int getBufferPercentage() {
        return 0;
    }

    @Override
    public int getCurrentPosition() {
        return player.getCurrentPosition();
    }

    @Override
    public int getDuration() {
        return player.getDuration();
    }

    @Override
    public boolean isPlaying() {
        return player.isPlaying();
    }

    @Override
    public void pause() {
        player.pause();
    }

    @Override
    public void seekTo(int i) {
        player.seekTo(i);
    }

    @Override
    public void start() {

        player.start();

    }

    @Override
    public boolean isFullScreen() {
        return false;
    }

    @Override
    public void toggleFullScreen() {

    }

    public MediaPlayer getMediaPlayer(){
        return player;
    }
// End VideoMediaController.MediaPlayerControl

    private MediaPlayer.OnPreparedListener onPreparedListener = new MediaPlayer.OnPreparedListener(){
        @Override
        public void onPrepared(MediaPlayer mp) {
            try{
                prepareDialog.dismiss();
            }catch (IllegalArgumentException e)
            {
                e.printStackTrace();
            }
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    start();
                }
            };

            Runnable runnable1 = new Runnable() {
                @Override
                public void run() {
                    player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            HashMap<String, String> moreProps = new HashMap<>();
                            moreProps.put("VIDEO_TITLE", videoTitle);
                            Utils.trackMixPanelEvent(mixpanel, Constants.MIXPANEL_EVENT_PLAY_VIDEO_FULL, moreProps);
                            goBackToParentActivity();
                        }
                    });

                }
            };
            Handler handler = new Handler();
            handler.postDelayed(runnable, 2000);
            handler.postDelayed(runnable1, 3000);

        }
    };





    private void goBackToParentActivity(){
        player.pause();
        player.stop();
        if(!player.isPlaying()) {
//            Intent intent = NavUtils.getParentActivityIntent(this);
//            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//            NavUtils.navigateUpTo(this, intent);
            NavUtils.navigateUpFromSameTask(this);
            finish();
        }
    }


    @Override
    public void onDestroy() {
        Log.i("WAQAS", "onDestroy: Video Player");
        super.onDestroy();
    }

// End VideoMediaController.MediaPlayerControl

}
