package com.continuum.activity;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;

import com.continuum.util.Constants;
import com.continuum.util.Utils;
import com.continuum.util.WindowUtil;

import com.mixpanel.android.mpmetrics.MixpanelAPI;


public class PublicationsFragment extends Fragment {
    private String curURL;
    private WebView webview = null;
    private Bundle webViewBundle;

    public void init(String url) {
        curURL = url;
    }

    public static PublicationsFragment newInstance(String idcko) {
        PublicationsFragment f = new PublicationsFragment();
        Bundle args = new Bundle();
        return f;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setRetainInstance(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_publications, container, false);


        if (webViewBundle != null) {
            webview.restoreState(webViewBundle);
        }

        MixpanelAPI mixpanel = MixpanelAPI.getInstance(getContext(), Constants.MIXPANEL_PROJECT_TOKEN);
        Utils.trackMixPanelEvent(mixpanel, Constants.MIXPANEL_EVENT_VIEW_PUBLICATIONS, null);
        Utils.flushMixPanel(mixpanel);


        if (curURL != null) {
            webview = (WebView) view.findViewById(R.id.publicationsWebView);
            Point screenUsableSize = WindowUtil.getScreenDimensions(getContext());

            int topBarHeight = Utils.getStatusBarHeight(getActivity());
            if (topBarHeight == 0) topBarHeight = Utils.getTitleBarHeight(getActivity());
//            if (topBarHeight == 0) topBarHeight = 50;
            topBarHeight = getStatusBarHeight();

            LinearLayout fragmentParent = (LinearLayout) getActivity().findViewById(R.id.fragment_placeholder_parent);
            fragmentParent.setPadding(0, topBarHeight, 0, 0);
            fragmentParent.setBackgroundColor(Color.parseColor("#000000"));
            fragmentParent.setVisibility(View.VISIBLE);

            WindowUtil.setLayoutHeight(webview, screenUsableSize.y - WindowUtil.getFooterLayoutDimensions(getContext()).y - topBarHeight);


            webview.getSettings().setJavaScriptEnabled(true);
            webview.setWebViewClient(new webClient());

            if (savedInstanceState == null) {
                webview.loadUrl(curURL);
            }
            if (savedInstanceState != null)
                webview.restoreState(savedInstanceState);
        }

        return view;

    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public void updateUrl(String url) {
        curURL = url;
        WebView webview = (WebView) getView().findViewById(R.id.publicationsWebView);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.loadUrl(url);
    }

    private class webClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return false;

        }
    }


}
