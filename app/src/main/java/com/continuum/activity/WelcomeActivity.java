package com.continuum.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.continuum.adapter.ViewPagerAdapter;
import com.continuum.model.WelcomeFinishEvent;

import de.greenrobot.event.EventBus;

public class WelcomeActivity extends BaseActivity implements ViewPager.OnPageChangeListener {

    protected View view;
    private ViewPager viewPager;
    private LinearLayout dots_indicator;
    private int dotsCount;
    private ImageView[] dots;
    private ViewPagerAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // To make activity full screen.
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);

        setReference();

    }

    @Override
    public void onResume(){
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause(){
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    public void onEvent(WelcomeFinishEvent event){
        finish();
    }



    @Override
    public void setReference() {
        view = LayoutInflater.from(this).inflate(R.layout.activity_welcome, container);

        dots_indicator = (LinearLayout) view.findViewById(R.id.viewPagerCountDots);

        mAdapter = new ViewPagerAdapter(WelcomeActivity.this);

        viewPager = (ViewPager) view.findViewById(R.id.pager_introduction);
        viewPager.setBackgroundResource(R.mipmap.welcome_bg);
//        viewPager.setBackgroundColor(0x352937);
        viewPager.setAdapter(mAdapter);
        viewPager.setCurrentItem(0);
        viewPager.addOnPageChangeListener(this);


        setUiPageViewController();
    }

    private void setUiPageViewController() {

        dotsCount = mAdapter.getCount();
        dots = new ImageView[dotsCount];

        for (int i = 0; i < dotsCount; i++) {
            dots[i] = new ImageView(this);
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.non_selected_item_dot));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams( LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            params.setMargins(4, 0, 4, 0);

            dots_indicator.addView(dots[i], params);
        }

        dots[0].setImageDrawable(getResources().getDrawable(R.drawable.selected_item_dot));
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        for (int i = 0; i < dotsCount; i++) {
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.non_selected_item_dot));
        }
        dots[position].setImageDrawable(getResources().getDrawable(R.drawable.selected_item_dot));
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }


    @Override
    public void onDestroy() {
        Log.i("WELCOME ACTIVITY", "onDestroy: Destroyed");
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        finish(); // finish activity
    }
}