package com.continuum.activity;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.continuum.adapter.VideoFeedsCustomAdapter;
import com.continuum.dto.Picture;
import com.continuum.dto.Video;
import com.continuum.exception.UncaughtExceptionHandler;
import com.continuum.util.Constants;
import com.continuum.util.TransParentProgressDialog;
import com.continuum.util.Utils;
import com.continuum.util.WindowUtil;
import com.continuum.view.DynamicListView;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.mixpanel.android.mpmetrics.MixpanelAPI;

public class VideoFeedsActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {

    private ArrayList<Video> videoList = new ArrayList<Video>();
    VideoFeedsCustomAdapter dataAdapter = null;
    SwipeRefreshLayout swipeLayout = null;

    public int PAGE_NO = 1;
    public int PAGE_SIZE = 2;
    public long TOTAL_VIDEOS = 0;
    public boolean loadingMore = false;

    private ImageView HOME_BUTTON;
    private ImageView EMAIL_BUTTON;
    private ImageView PUBLICATIONS_BUTTON;
    private ImageView HOME_BUTTON_BG;
    private ImageView EMAIL_BUTTON_BG;
    private ImageView PUBLICATIONS_BUTTON_BG;



    private ProgressDialog pStreamDialog;
//    private  TransParentProgressDialog transParentDialog;
    private String currentScreen = "HOME";
    private String previousScreen = "HOME";

    private Context context;
    boolean alreadyLoggedInMixPanel = true;
    DynamicListView listView = null;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        try {

            super.onCreate(savedInstanceState);
            pStreamDialog = new ProgressDialog(this,R.style.ProgreessTheme);
            pStreamDialog.setIndeterminate(false);
            pStreamDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pStreamDialog.setCancelable(false);
            Utils.firstTimeLoadFlag = false;
            if (savedInstanceState == null) {
                Utils.trackMixPanelEvent(mixpanel, Constants.MIXPANEL_EVENT_OPEN, null);
                alreadyLoggedInMixPanel = false;
            } else {
//                Log.i("WAQAS", "Retain State: " + listView);
//                savedInstanceState.getParcelable("state");
//                listView = new DynamicListView(this);
//                listView.onRestoreInstanceState(savedInstanceState.getParcelable("state"));
//
//                dataAdapter = (VideoFeedsCustomAdapter) listView.getAdapter();

            }

            Thread.setDefaultUncaughtExceptionHandler(new UncaughtExceptionHandler(this));
            SharedPreferences prefs = getSharedPreferences(Constants.SHARED_PREF_TITLE, this.MODE_PRIVATE);

            boolean firstTime = prefs.getBoolean(Constants.SHARED_PREF_FLAG, true);
            if (firstTime) {
                context = this;
                Intent welcomeActivity = new Intent(this, WelcomeActivity.class);
                welcomeActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);

                Utils.trackMixPanelEvent(mixpanel, Constants.MIXPANEL_EVENT_APP_INSTALL, null);

                startActivity(welcomeActivity);

            } else {
                context = this;
                setContentView(R.layout.activity_video_feeds);

                Utils.setStatusBarTranslucent(true, this);

                swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
                swipeLayout.setOnRefreshListener(this);
                swipeLayout.setColorSchemeColors(R.color.colorBlack, R.color.colorLightGray);
                swipeLayout.setNestedScrollingEnabled(true);


                View footerLayout = findViewById(R.id.footerLayout);

                HOME_BUTTON = (ImageView) footerLayout.findViewById(R.id.icon_tv);
                EMAIL_BUTTON = (ImageView) footerLayout.findViewById(R.id.icon_buddy);
                PUBLICATIONS_BUTTON = (ImageView) footerLayout.findViewById(R.id.icon_feed);
                HOME_BUTTON_BG = (ImageView) footerLayout.findViewById(R.id.icon_tv_bg_img);
                EMAIL_BUTTON_BG = (ImageView) footerLayout.findViewById(R.id.icon_buddy_bg_img);
                PUBLICATIONS_BUTTON_BG = (ImageView) footerLayout.findViewById(R.id.icon_feed_bg_img);


                HOME_BUTTON.setOnTouchListener(mHomeTouchListener);
                HOME_BUTTON.setBackgroundResource(R.drawable.active_btn_light);
                HOME_BUTTON_BG.setVisibility(View.VISIBLE);


                EMAIL_BUTTON.setOnTouchListener(mEmailTouchListener);
                PUBLICATIONS_BUTTON.setOnTouchListener(mPublicationsTouchListener);



//                    if(listView==null) {
                listView = (DynamicListView) findViewById(R.id.listView);
                dataAdapter = new VideoFeedsCustomAdapter(this, R.layout.video_info, videoList, mixpanel);
                listView.setAdapter(dataAdapter);

                listView.setContext(this);
                grabURL((Constants.VIDEO_CHANNEL_URL + "&page=" + (PAGE_NO) + "&per_page=" + PAGE_SIZE), "VideoFeedsActivity");
                Log.i("FeedsActivity", "onCreate");
//                    }

                Point screenUsableSize = WindowUtil.getScreenDimensions(context);
                View videoFeedsLayout = findViewById(R.id.videoFeedsParentLayout);

                if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                    WindowUtil.setLayoutHeight(videoFeedsLayout, screenUsableSize.y);
                } else {
                    WindowUtil.setLayoutWidth(videoFeedsLayout, screenUsableSize.x);
                }

            }


        } catch (Exception e) {
            Utils.trackMixPanelEvent(mixpanel, Constants.MIXPANEL_EVENT_ERROR, Utils.getErrorStackTraceHashMap(e));
            Utils.flushMixPanel(mixpanel);
            e.printStackTrace();
        } finally {
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (dataAdapter != null && videoList.size() > 0) {

        }
    }

    public void grabURL(String url, String source) {
        new GrabURL(source).execute(url);
    }

    private class GrabURL extends AsyncTask<String, Void, String> {
        String source;
        private String content = null;
        private boolean error = false;

        public GrabURL(String source) {
            this.source = source;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pStreamDialog.setMessage("Loading...");
            if (Utils.firstTimeLoadFlag == false) {
                pStreamDialog.show();
                Utils.firstTimeLoadFlag = true;
            }

        }
        protected String doInBackground(String... urls) {
            loadingMore = true;
            HttpURLConnection urlConnection = null;
            InputStream inputStr = null;
            try {
                URL myURL = new URL(urls[0]);
                urlConnection = (HttpURLConnection) myURL.openConnection();
                urlConnection.setConnectTimeout(Constants.CONNECTION_TIMEOUT);
                urlConnection.setReadTimeout(Constants.WAIT_TIMEOUT);
                urlConnection.setRequestProperty("Authorization", Constants.AUTH_HEADER);
                urlConnection.setRequestMethod("GET");
                urlConnection.setRequestProperty("Content-Type", Constants.CONTENT_TYPE);
                urlConnection.setUseCaches(true);

                int responseCode = urlConnection.getResponseCode();
                if (responseCode == 200) {
                    PAGE_NO++;
                    inputStr = urlConnection.getInputStream();
                    String encoding = urlConnection.getContentEncoding() == null ? "UTF-8" : urlConnection.getContentEncoding();
                    content = IOUtils.toString(inputStr, encoding);
                }

            } catch (IOException e) {
                Utils.trackMixPanelEvent(mixpanel, Constants.MIXPANEL_EVENT_ERROR, Utils.getErrorStackTraceHashMap(e));
                Utils.flushMixPanel(mixpanel);

                Log.w("HTTP3:", e);
                content = e.getMessage();
                error = true;
                cancel(true);
                loadingMore = false;
            } catch (Exception e) {
                Utils.trackMixPanelEvent(mixpanel, Constants.MIXPANEL_EVENT_ERROR, Utils.getErrorStackTraceHashMap(e));
                Utils.flushMixPanel(mixpanel);


                Log.w("HTTP4:", e);
                content = e.getMessage();
                error = true;
                cancel(true);
                loadingMore = false;
            } finally {
                if (urlConnection != null) urlConnection.disconnect();
                if (inputStr != null) {
                    try {
                        inputStr.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            return content;
        }

        @Override
        protected void onPostExecute(String content) {
            pStreamDialog.dismiss();
            if (error) {
                Toast toast = Toast.makeText(VideoFeedsActivity.this, "Error! Please try to reload the application or come back later.", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 25, 200);
                toast.show();

                Utils.trackMixPanelEvent(mixpanel, Constants.MIXPANEL_EVENT_ERROR, null);
                Utils.flushMixPanel(mixpanel);
            } else {
                addVideosToAdapterList(content, source);
            }
        }

    }

    private void addVideosToAdapterList(String response, String source) {
        Log.i("FeedsActivity", "addVideosToAdapterList" + ": " + response);
        JSONObject responseObj = null;
        try {
            responseObj = new JSONObject(response);
            String total = responseObj.getString("total");
            TOTAL_VIDEOS = Long.valueOf(total);
            JSONArray dataObj = responseObj.getJSONArray("data");

            if (dataObj != null && dataObj.length() > 0) {
                boolean videosAdded = false;
                for (int i = 0; i < dataObj.length(); i++) {

                    //get the video information JSON object
                    String status = dataObj.getJSONObject(i).getString("status");
                    boolean isAvailable = false;
                    if (status.equalsIgnoreCase("available")) {
                        isAvailable = true;
                    }
                    if (isAvailable) {
                        String name = dataObj.getJSONObject(i).getString("name");
                        String link = dataObj.getJSONObject(i).getString("link");
                        String description = dataObj.getJSONObject(i).getString("description");
                        String duration = dataObj.getJSONObject(i).getString("duration");

                        if (i == 0 && source.equalsIgnoreCase("VideoFeedsActivity") && !alreadyLoggedInMixPanel) {
                            //For first item, this is the only way to record View Video Cover event.
                            //i==0 check is important because first call from VideoFeedsActivity has 2 items in the call..
                             /*MIX PANEL EVENT TRACKING*/
                            HashMap<String, String> moreProps = new HashMap<>();
                            moreProps.put("VIDEO_TITLE", name);
                            Utils.trackMixPanelEvent(mixpanel, Constants.MIXPANEL_EVENT_VIEW_VIDEO_COVER, moreProps);
                            /*MIX PANEL EVENT TRACKING*/

                        }


                        JSONObject metaData = dataObj.getJSONObject(i).getJSONObject("metadata");
                        JSONObject metaDataConnections = metaData.getJSONObject("connections");
                        JSONObject metaDataCredtis = metaDataConnections.getJSONObject("credits");
                        String creditsURI = metaDataCredtis.getString("uri");


                        String uri = null;
                        JSONArray streamingURIArray = dataObj.getJSONObject(i).getJSONArray("files");

                        String fetchQuality = Constants.VIDEO_QUALITY_HLS;
                        if (android.os.Build.VERSION.SDK_INT <= 21) {
                            fetchQuality = Constants.VIDEO_QUALITY_HD;
                        }
                        Log.i("WAQAS", "addVideosToAdapterList: " + fetchQuality);
                        for (int j = 0; j < streamingURIArray.length(); j++) {
                            String quality = streamingURIArray.getJSONObject(j).getString("quality");
                            if (quality.equalsIgnoreCase(fetchQuality)) {
                                uri = streamingURIArray.getJSONObject(j).getString("link_secure"); //link_secure for https
                            }
                        }
                        if (uri == null) {
                            for (int j = 0; j < streamingURIArray.length(); j++) {
                                String quality = streamingURIArray.getJSONObject(j).getString("quality");
                                if (quality.equalsIgnoreCase(Constants.VIDEO_QUALITY_HD)) {
                                    uri = streamingURIArray.getJSONObject(j).getString("link_secure");
                                }
                            }

                            if (uri == null)
                                uri = streamingURIArray.getJSONObject(0).getString("link_secure");
                        }

                        //create java object from the JSON object
                        Video video = new Video(uri, name, description, link, Double.valueOf(duration));
                        video.setCreditsUri(Constants.BASE_URL + creditsURI);

                        JSONObject picturesObject = dataObj.getJSONObject(i).getJSONObject("pictures");
                        JSONArray pictureArray = picturesObject.getJSONArray("sizes");
                        JSONObject picObj = pictureArray.getJSONObject(pictureArray.length() - 1);
                        String pictureLink = picObj.getString("link");
                        String pictureWidth = picObj.getString("width");
                        String pictureHeight = picObj.getString("height");
                        video.setPicture(new Picture(pictureLink, pictureWidth, pictureHeight, null));

                        videosAdded = addVideosWithCheck(video);
                        grabCreditsURL(video);
                    }
                }

                if (videosAdded)
                    dataAdapter.notifyDataSetChanged();

                loadingMore = false;

            }

        } catch (Exception e) {
            loadingMore = false;
            Utils.trackMixPanelEvent(mixpanel, Constants.MIXPANEL_EVENT_ERROR, Utils.getErrorStackTraceHashMap(e));
            Utils.flushMixPanel(mixpanel);


        } finally {

        }

    }

    public boolean addVideosWithCheck(Video video) {
        if (videoList.size() == 0) {
            videoList.add(video);
            return true;
        } else {
            boolean bAdded = false;
            for (int i = 0; i < videoList.size(); i++) {
                Video tempVideo = videoList.get(i);
                if (!tempVideo.getUri().equals(video.getUri())) {
                    videoList.add(video);
                    bAdded = true;
                    break;
                }
            }
            return bAdded;
        }
    }

    public void grabCreditsURL(Video video) {
        new GrabCreditsURL().execute(video);
    }

    private class GrabCreditsURL extends AsyncTask<Video, Void, String> {

        private String content = null;
        Video video = null;
        private boolean error = false;

        protected String doInBackground(Video... videos) {
            loadingMore = true;
            HttpURLConnection urlConnection = null;
            InputStream inputStr = null;

            video = videos[0];
            try {
                URL myURL = new URL(video.getCreditsUri());
                urlConnection = (HttpURLConnection) myURL.openConnection();
                urlConnection.setConnectTimeout(Constants.CONNECTION_TIMEOUT);
                urlConnection.setReadTimeout(Constants.WAIT_TIMEOUT);
                urlConnection.setRequestProperty("Authorization", Constants.AUTH_HEADER);
                urlConnection.setRequestMethod("GET");
                urlConnection.setRequestProperty("Content-Type", Constants.CONTENT_TYPE);
                urlConnection.setUseCaches(true);

                int responseCode = urlConnection.getResponseCode();
                if (responseCode == 200) {
                    inputStr = urlConnection.getInputStream();
                    String encoding = urlConnection.getContentEncoding() == null ? "UTF-8" : urlConnection.getContentEncoding();
                    content = IOUtils.toString(inputStr, encoding);
                }

            } catch (IOException e) {

                Utils.trackMixPanelEvent(mixpanel, Constants.MIXPANEL_EVENT_ERROR, Utils.getErrorStackTraceHashMap(e));
                Utils.flushMixPanel(mixpanel);

                Log.w("HTTP3:", e);
                content = e.getMessage();
                error = true;
                cancel(true);
                loadingMore = false;
            } catch (Exception e) {
                Utils.trackMixPanelEvent(mixpanel, Constants.MIXPANEL_EVENT_ERROR, Utils.getErrorStackTraceHashMap(e));
                Utils.flushMixPanel(mixpanel);


                Log.w("HTTP4:", e);
                content = e.getMessage();
                error = true;
                cancel(true);
                loadingMore = false;
            } finally {
                if (urlConnection != null) urlConnection.disconnect();
                if (inputStr != null) {
                    try {
                        inputStr.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }

            return content;
        }


        protected void onPostExecute(String content) {

            if (error) {
                Toast toast = Toast.makeText(VideoFeedsActivity.this, "Error! Please try to reload the application or come back later.", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.TOP, 25, 400);
                toast.show();

                Utils.trackMixPanelEvent(mixpanel, Constants.MIXPANEL_EVENT_ERROR, null);
            } else {
                addCreditsToVideo(content, video);
            }
        }

    }

    private void addCreditsToVideo(String response, Video video) {
        JSONObject responseObj = null;
        try {
            responseObj = new JSONObject(response);
            JSONArray dataObj = responseObj.getJSONArray("data");
            boolean creditsAdded = false;
            HashMap<String, String> credits = new HashMap<String, String>();
            if (dataObj != null && dataObj.length() > 0) {
                for (int i = 0; i < dataObj.length(); i++) {
                    if (true) {
                        String role = dataObj.getJSONObject(i).getString("role");
                        String name = dataObj.getJSONObject(i).getString("name");
                        credits.put(role, name);
                        creditsAdded = true;
                        break;
                    }
                }
                if (creditsAdded) {
                    video.setCredits(credits);
                }
            }
//            dataAdapter.notifyDataSetChanged();

        } catch (Exception e) {
            loadingMore = false;
            Utils.trackMixPanelEvent(mixpanel, Constants.MIXPANEL_EVENT_ERROR, Utils.getErrorStackTraceHashMap(e));
            Utils.flushMixPanel(mixpanel);

        } finally {
        }


    }

    @Override
    public void onDestroy() {
        Log.i("WAQAS", "onDestroy: Video Feeds");
//        Utils.flushMixPanel(mixpanel);
        super.onDestroy();
    }

    private boolean exit = false;

    @Override
    public void onBackPressed() {
        if (exit) {
            finish();
        } else {
            Toast.makeText(this, Constants.EXIT_MESSAGE, Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);
            Utils.flushMixPanel(mixpanel);
        }

    }

    private View.OnTouchListener mHomeTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            previousScreen = currentScreen;
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                Utils.highlightBG(HOME_BUTTON, EMAIL_BUTTON, PUBLICATIONS_BUTTON);
                HOME_BUTTON_BG.setVisibility(View.INVISIBLE);

                return true;
            } else if (event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL) {
                Utils.highlightBG(HOME_BUTTON, EMAIL_BUTTON, PUBLICATIONS_BUTTON);
                Utils.displayHighlightedBG(HOME_BUTTON_BG, EMAIL_BUTTON_BG, PUBLICATIONS_BUTTON_BG);

                if (!currentScreen.equalsIgnoreCase("HOME")) {
                    getFragmentManager().popBackStack();
                    closeAllFragments();
//                    Utils.unlockRotation(VideoFeedsActivity.this);

                }

                currentScreen = "HOME";

                return true;
            }

            return false;
        }
    };

    private View.OnTouchListener mEmailTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            previousScreen = currentScreen;
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                Utils.highlightBG(EMAIL_BUTTON, HOME_BUTTON, PUBLICATIONS_BUTTON);
                EMAIL_BUTTON_BG.setVisibility(View.INVISIBLE);

                return true;
            } else if (event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL) {
                Utils.highlightBG(EMAIL_BUTTON, HOME_BUTTON, PUBLICATIONS_BUTTON);
                Utils.displayHighlightedBG(EMAIL_BUTTON_BG, HOME_BUTTON_BG, PUBLICATIONS_BUTTON_BG);

                if (!currentScreen.equalsIgnoreCase("EMAIL")) {
                    Utils.openEmailClient((Activity) context);
                    return true;
                }

                currentScreen = "EMAIL";
                return true;
            }

            return false;
        }
    };

    private View.OnTouchListener mPublicationsTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            previousScreen = currentScreen;
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                Utils.highlightBG(PUBLICATIONS_BUTTON, HOME_BUTTON, EMAIL_BUTTON);
                PUBLICATIONS_BUTTON_BG.setVisibility(View.INVISIBLE);
                return true;
            } else if (event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL) {
                Utils.highlightBG(PUBLICATIONS_BUTTON, HOME_BUTTON, EMAIL_BUTTON);
                Utils.displayHighlightedBG(PUBLICATIONS_BUTTON_BG, HOME_BUTTON_BG, EMAIL_BUTTON_BG);

                if (!currentScreen.equalsIgnoreCase("PUBLICATIONS")) {
                    Utils.openPublicationsFragment(VideoFeedsActivity.this);
                }

                currentScreen = "PUBLICATIONS";
                return true;
            }

            return false;
        }
    };


    private void closeAllFragments() {
        try {
            FragmentManager fm = getSupportFragmentManager();
            List<Fragment> fragments = fm.getFragments();
            for (Fragment fragment : fragments) {
                fm.popBackStackImmediate();
            }

            LinearLayout fragmentParent = (LinearLayout) findViewById(R.id.fragment_placeholder_parent);
            fragmentParent.setVisibility(View.GONE);


        } catch (Exception e) {
            HashMap<String, String> moreProps = Utils.getErrorStackTraceHashMap(e);

            Utils.trackMixPanelEvent(mixpanel, Constants.MIXPANEL_EVENT_ERROR, moreProps);
            Utils.flushMixPanel(mixpanel);

        } finally {
        }

    }

    //** When email activity fragment is closed, we receive result here...
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.EMAIL_SENDER_ACTIVITY_REQ_CODE) {
            if (resultCode == RESULT_OK || resultCode == RESULT_CANCELED) {
                if (previousScreen.equalsIgnoreCase("HOME")) {
                    HOME_BUTTON.setBackgroundResource(R.drawable.active_btn_light);
                    EMAIL_BUTTON.setBackgroundResource(0);
                    PUBLICATIONS_BUTTON.setBackgroundResource(0);

                    HOME_BUTTON_BG.setVisibility(View.VISIBLE);
                    EMAIL_BUTTON_BG.setVisibility(View.INVISIBLE);
                    PUBLICATIONS_BUTTON_BG.setVisibility(View.INVISIBLE);

                }
                if (previousScreen.equalsIgnoreCase("PUBLICATIONS")) {
                    HOME_BUTTON.setBackgroundResource(0);
                    EMAIL_BUTTON.setBackgroundResource(0);
                    PUBLICATIONS_BUTTON.setBackgroundResource(R.drawable.active_btn_light);

                    HOME_BUTTON_BG.setVisibility(View.INVISIBLE);
                    EMAIL_BUTTON_BG.setVisibility(View.INVISIBLE);
                    PUBLICATIONS_BUTTON_BG.setVisibility(View.VISIBLE);

                }
            }
        } else if (requestCode == Constants.VIDEOPLAYER_ACTIVITY_REQ_CODE) {
            Log.i("WAQAS", "onActivityResult: RETURNED");
        }
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeLayout.setRefreshing(false);
            }
        }, 700);
    }
/*
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {

        Parcelable state = listView.onSaveInstanceState();
        savedInstanceState.putParcelable("state", state);
        listView.setAdapter(dataAdapter);

        super.onSaveInstanceState(savedInstanceState);

    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        listView = new DynamicListView(this);
        listView.onRestoreInstanceState(savedInstanceState.getParcelable("state"));
        dataAdapter = (VideoFeedsCustomAdapter) listView.getAdapter();

    }


*/

}
