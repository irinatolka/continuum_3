package com.continuum.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;

import com.continuum.activity.R;
import com.continuum.util.Constants;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

public class BaseActivity extends AppCompatActivity {

    public FrameLayout container;
    public CoordinatorLayout mainlayout;
    public MixpanelAPI mixpanel = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mixpanel = MixpanelAPI.getInstance(this, Constants.MIXPANEL_PROJECT_TOKEN);

        setContentView(R.layout.activity_base);
        container = (FrameLayout) findViewById(R.id.container);
        mainlayout = (CoordinatorLayout) findViewById(R.id.fullLayout);
    }

    // Method to set xml object reference.
    public void setReference(){

    }

    @Override
    protected void onDestroy() {
        Log.i("BASE ACTIVITY", "onDestroy: Destroyed" );
        super.onDestroy();
    }

    public MixpanelAPI getMixpanel(){
        return mixpanel;
    }
}
