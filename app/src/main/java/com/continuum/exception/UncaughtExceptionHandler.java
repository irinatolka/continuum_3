package com.continuum.exception;

import android.app.Activity;
import android.content.Context;

import com.continuum.activity.VideoFeedsActivity;
import com.continuum.util.Constants;
import com.continuum.util.Utils;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by waqas.memon on 2/13/2016.
 */

public class UncaughtExceptionHandler implements Thread.UncaughtExceptionHandler {

    private Thread.UncaughtExceptionHandler defaultUEH;

    private Activity app = null;

    public UncaughtExceptionHandler(Activity app) {
        this.defaultUEH = Thread.getDefaultUncaughtExceptionHandler();
        this.app = app;
    }

    public void uncaughtException(Thread t, Throwable e) {
        e.printStackTrace();
        defaultUEH.uncaughtException(t, e);
    }
}