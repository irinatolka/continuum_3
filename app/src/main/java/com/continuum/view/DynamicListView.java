package com.continuum.view;

/**
 * Created by waqas.memon on 2/13/2016.
 */

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ViewConfiguration;
import android.widget.ListView;

import com.continuum.activity.VideoFeedsActivity;
import com.continuum.adapter.VideoFeedsCustomAdapter;
import com.continuum.util.Constants;
import com.continuum.util.Utils;

import java.util.HashMap;

/**
 * Created by paveld on 1/13/14.
 */
public class DynamicListView extends ListView {

    private float touchSlop = 0;
    private float totalDistanceToWaitFor = 200;
    private float downY = 0;
    private boolean consumeTouchEvents = false;

    private int firstVisible = 0;
    private float startX;
    private float startY;
    private float endX;
    private float endY;
    private String direction;

    VideoFeedsActivity parentActivity;

    public DynamicListView(Context context) {
        super(context);
        init();
    }

    public DynamicListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DynamicListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        touchSlop = ViewConfiguration.get(getContext()).getScaledTouchSlop();
        firstVisible = getFirstVisiblePosition();
    }
    public void setContext(VideoFeedsActivity parentActivity) {
        this.parentActivity = parentActivity;
    }
    /////////////
    float nPrevTouchY = 0, deltaY = 0;
    /////////////////


    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        boolean isHandled = true;
        float distance = downY - ev.getY();
        switch (ev.getActionMasked()) {
            case MotionEvent.ACTION_MOVE:
                Log.e("ListView", "action_move");
                deltaY =  nPrevTouchY - ev.getY();
                nPrevTouchY = ev.getY();
                Log.e("Touch", "detlaY: = "+ String.valueOf(deltaY));
                Log.e("Touch", "nPrevY: = "+ String.valueOf(nPrevTouchY));
                smoothScrollBy((int)deltaY, 1);
                break;
            case MotionEvent.ACTION_UP:
                Log.e("ListView", "action_up");
                handleScroll(distance, 300);
                break;
            case MotionEvent.ACTION_CANCEL:
                Log.e("ListView", "action_cancel");
                handleScroll(distance,300);
                break;

            case MotionEvent.ACTION_DOWN:
                Log.e("ListView", "action_down");
                nPrevTouchY = ev.getY();
                if (ev.getActionMasked() == MotionEvent.ACTION_DOWN) {
                    startX = ev.getX();
                    startY = ev.getY();
                    consumeTouchEvents = false;
                    downY = ev.getY();
                }
                break;
                //fallthrough
            default:
                break;

        }
        return isHandled;
    }


    private void handleScroll(float distance, float totalDistanceToWaitFor) {
        try {

            if (Math.abs(distance) > totalDistanceToWaitFor) {
                if (distance > 0){ // next
                    firstVisible++;
                    if (firstVisible >= getAdapter().getCount() - 1) {
                        int pageNo = parentActivity.PAGE_NO + 1;
                        int pageSize = parentActivity.PAGE_SIZE;
                        if (pageNo <= parentActivity.TOTAL_VIDEOS) {

                            String url = Constants.VIDEO_CHANNEL_URL + "&page=" + (pageNo) + "&per_page=" + (pageSize);
                            parentActivity.loadingMore = true;
                            parentActivity.grabURL(url, "DynamicListView");
                        }
                        firstVisible = getAdapter().getCount() - 1;
                    }
                    smoothScrollToPositionFromTop(firstVisible, 0, (int)totalDistanceToWaitFor);
                }else{ // prew
                    if (firstVisible > 0) {
                        firstVisible--;
                    }
                    smoothScrollToPositionFromTop(firstVisible, 0, (int)totalDistanceToWaitFor);
                }

            } else {
                if (distance >0){//next
                    smoothScrollToPositionFromTop(firstVisible, 0, (int)totalDistanceToWaitFor );
                }else{
                    smoothScrollToPositionFromTop(firstVisible, 0, (int)totalDistanceToWaitFor  );
                }
            }

            VideoFeedsCustomAdapter adapter = (VideoFeedsCustomAdapter) getAdapter();
            if (adapter != null && adapter.videoList != null && adapter.videoList.size() > (firstVisible)) {
                /*MIX PANEL EVENT TRACKING*/
                HashMap<String, String> moreProps = new HashMap<>();
                moreProps.put("VIDEO_TITLE", adapter.videoList.get(firstVisible).getName());
                Utils.trackMixPanelEvent(parentActivity.mixpanel, Constants.MIXPANEL_EVENT_VIEW_VIDEO_COVER, moreProps);
                /*MIX PANEL EVENT TRACKING*/
            }
        }catch(Exception e){
            Utils.trackMixPanelEvent(parentActivity.mixpanel, Constants.MIXPANEL_EVENT_ERROR, Utils.getErrorStackTraceHashMap(e));
            Utils.flushMixPanel(parentActivity.mixpanel);
        } finally{
        }


    }




}