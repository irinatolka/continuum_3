package com.continuum.view;

import android.content.Context;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.continuum.activity.R;
import com.continuum.activity.VideoFeedsActivity;
import com.continuum.util.WindowUtil;
import com.loopj.android.image.SmartImageView;

/**
 * Created by waqas.memon on 2/7/2016.
 */
public class DynamicImageView extends SmartImageView {
    private Context context;

    public DynamicImageView(Context context) {
        super(context);
    }

    public DynamicImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public DynamicImageView(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    @Override
    protected void onMeasure(final int widthMeasureSpec, final int heightMeasureSpec) {
        final Drawable d = this.getDrawable();
        if (d != null) {

            Point screenUsableSize = WindowUtil.getScreenDimensions(context);
            this.setMeasuredDimension(screenUsableSize.x, screenUsableSize.y);

        } else {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }

}