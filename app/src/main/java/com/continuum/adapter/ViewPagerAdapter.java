package com.continuum.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.continuum.activity.R;
import com.continuum.activity.VideoFeedsActivity;
import com.continuum.activity.WelcomeActivity;
import com.continuum.model.WelcomeFinishEvent;
import com.continuum.util.Constants;

import de.greenrobot.event.EventBus;

/**
 * Created by Wasim on 11-06-2015.
 */
public class ViewPagerAdapter extends PagerAdapter{

    private Context mContext;

    private String[] welcomeHeaders = {
        Constants.WELCOME_SCREEN_1_TITLE,
        Constants.WELCOME_SCREEN_2_TITLE,
        Constants.WELCOME_SCREEN_3_TITLE,
        Constants.WELCOME_SCREEN_4_TITLE,
        Constants.WELCOME_SCREEN_5_TITLE,
    };

    private String[] welcomeDescriptions = {
        Constants.WELCOME_SCREEN_1_DETAILS,
        Constants.WELCOME_SCREEN_2_DETAILS,
        Constants.WELCOME_SCREEN_3_DETAILS,
        Constants.WELCOME_SCREEN_4_DETAILS,
        Constants.WELCOME_SCREEN_5_DETAILS,
    };

    private int position;
    private View itemView;
    private TextView titleTextView;
    private TextView descTextView;

    public ViewPagerAdapter(Context mContext) {
        this.mContext = mContext;
    }


    @Override
    public int getCount() {
        return this.welcomeHeaders.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }


    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        itemView = LayoutInflater.from(mContext).inflate(R.layout.pager_item, container, false);

        titleTextView = (TextView) itemView.findViewById(R.id.pager_item_title);
        titleTextView.setText(welcomeHeaders[position]);
        titleTextView.setAllCaps(true);

        descTextView = (TextView) itemView.findViewById(R.id.pager_item_desc);
        descTextView.setText(welcomeDescriptions[position]);

        int height = getScreenHeight();

        titleTextView.setPadding(50, ((height / 3)-70), 50, 0);
//        descTextView.setPadding(50, 30, 50,0);

        if(position == (welcomeHeaders.length-1)){
            titleTextView.setVisibility(View.GONE);
            descTextView.setVisibility(View.GONE);

            ImageView logoImg = (ImageView) itemView.findViewById(R.id.logo);
            logoImg.setVisibility(View.VISIBLE);
            logoImg.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            logoImg.setPadding(100, ((height / 3)-200), 100, 0);

            logoImg.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    SharedPreferences prefs = mContext.getSharedPreferences(Constants.SHARED_PREF_TITLE, mContext.MODE_PRIVATE);
                    SharedPreferences.Editor ed = prefs.edit();
                    ed.putBoolean(Constants.SHARED_PREF_FLAG, false);
                    ed.commit();

                    Intent videoFeedsIntent = new Intent(mContext, VideoFeedsActivity.class);
                    videoFeedsIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    mContext.startActivity(videoFeedsIntent);
//                    EventBus.getDefault().post(new WelcomeFinishEvent(true));
                }
            });
        }
        container.addView(itemView);

        return itemView;
    }

    private int getScreenHeight() {
        Display display = ((WelcomeActivity) mContext).getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        return height;
    }

  /*  @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        LinearLayout obj = (LinearLayout) object;
        container.removeView(obj);
    }*/

    @Override
    public void destroyItem(View collection, int position, Object o) {
        View view = (View)o;
        ((ViewPager) collection).removeView(view);
        view = null;
    }
}
