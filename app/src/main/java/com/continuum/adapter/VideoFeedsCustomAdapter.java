package com.continuum.adapter;

import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.continuum.activity.PublicationsFragment;
import com.continuum.activity.R;
import com.continuum.activity.VideoFeedsActivity;
import com.continuum.activity.VideoPlayerActivity;
import com.continuum.dto.Video;
import com.continuum.util.Constants;
import com.continuum.util.Utils;
import com.continuum.util.WindowUtil;
import com.continuum.view.DynamicImageView;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by waqas.memon on 2/1/2016.
 */
public class VideoFeedsCustomAdapter extends ArrayAdapter<Video> {

    public ArrayList<Video> videoList;
    Context context;
    MixpanelAPI mixpanelAPI;

    public VideoFeedsCustomAdapter(Context context, int textViewResourceId, ArrayList<Video> videoList, MixpanelAPI mixpanelAPI) {
        super(context, textViewResourceId, videoList);
        this.context = context;
        this.videoList = videoList;
        this.mixpanelAPI = mixpanelAPI;
    }

    private class ViewHolder {
        TextView video_title;
        TextView videoContributor;
        DynamicImageView video_image;
        ImageView playIcon;
        FrameLayout feedsControlLayer;
        LinearLayout playIconLayout;
        Spinner spinnerloading;

    }

    public void add(Video video){
        this.videoList.add(video);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {

            LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.video_info, parent, false);

            holder = new ViewHolder();
            holder.playIcon = (ImageView) convertView.findViewById(R.id.playIcon);

            holder.video_image = (DynamicImageView) convertView.findViewById(R.id.video_image);
            holder.video_title= (TextView) convertView.findViewById(R.id.video_title);
            holder.videoContributor = (TextView) convertView.findViewById(R.id.video_contributor);
            holder.feedsControlLayer = (FrameLayout) convertView.findViewById(R.id.feedsControlLayer);
            holder.playIconLayout = (LinearLayout) convertView.findViewById(R.id.playIconLayout);
            holder.spinnerloading = (Spinner)convertView.findViewById(R.id.spinierloading);
     //       holder.spinnerloading.setAdapter(VideoFeedsCustomAdapter.this);
            convertView.setTag(holder);


        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Video video = this.videoList.get(position);
        final String videoName = video.getName();
        final String streamingURL = video.getUri();

        StringBuilder contributor = new StringBuilder();

        if(video.getCredits()!=null) {
            for (String key : video.getCredits().keySet()) {
                if (key == null || key.equalsIgnoreCase("null") || key.trim().equalsIgnoreCase("") ||
                        video.getCredits().get(key)==null || video.getCredits().get(key).equalsIgnoreCase("null") ||
                        video.getCredits().get(key).trim().equalsIgnoreCase("")) {

                    contributor = new StringBuilder("");

                }else {
                    contributor.append(key).append(": ").append(video.getCredits().get(key));
                }
                break;
            }
        }

        holder.video_title.setText(videoName);
        holder.videoContributor.setText(contributor.toString());
//        holder.video_image.setImageUrl(video.getPicture().getPictureLink());
//        holder.video_image.setScaleType(ImageView.ScaleType.CENTER_CROP);
        Picasso.with(context).load(video.getPicture().getPictureLink()).placeholder(R.drawable.bg_placeholder).into(holder.video_image);


        holder.feedsControlLayer.setVisibility(View.VISIBLE);
        AlphaAnimation fadeIn = new AlphaAnimation(0.0f, 1.0f);
        fadeIn.setDuration(700);
        fadeIn.setStartOffset(200);
        holder.feedsControlLayer.setAnimation(fadeIn);

        holder.playIcon.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        ImageView view = (ImageView) v;
                        view.getDrawable().setColorFilter(0x77000000, PorterDuff.Mode.SRC_ATOP);
                        view.invalidate();
                        break;
                    }
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_CANCEL: {
                        Toast toast = Toast.makeText(context, "Loading...", Toast.LENGTH_SHORT);
//                        toast.setGravity(Gravity.CENTER, 25, 200);
//                        toast.show();

                        ImageView view = (ImageView) v;
                        view.getDrawable().clearColorFilter();
                        view.invalidate();
                        try {


                            /*MIX PANEL EVENT TRACKING*/
                            HashMap<String, String> moreProps = new HashMap<>();
                            VideoFeedsActivity parentActivity = (VideoFeedsActivity) context;
                            TextView textView = (TextView) parentActivity.findViewById(R.id.video_title);
                            if (textView != null) {
                                String title = (String) textView.getText();
                                moreProps.put("VIDEO_TITLE", title);
                            }
                            Utils.trackMixPanelEvent(parentActivity.mixpanel, Constants.MIXPANEL_EVENT_PLAY_VIDEO, moreProps);
                            /*MIX PANEL EVENT TRACKING*/

                            Bundle b = new Bundle();
                            b.putString("videoURL", streamingURL);
                            b.putString("videoTitle", videoName);
                            Intent intent = new Intent(context, VideoPlayerActivity.class);
                            intent.putExtras(b);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                            ((VideoFeedsActivity) context).startActivityForResult(intent, Constants.VIDEOPLAYER_ACTIVITY_REQ_CODE);
                        } catch (Exception e) {
                            e.printStackTrace();
                            toast = Toast.makeText(context, "Error! Please try to reload the application or come back later.", Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.TOP, 25, 200);
                            toast.show();

                            VideoFeedsActivity parentActivity = (VideoFeedsActivity) context;
                            Utils.trackMixPanelEvent(parentActivity.mixpanel, Constants.MIXPANEL_EVENT_ERROR, Utils.getErrorStackTraceHashMap(e));
                            Utils.flushMixPanel(parentActivity.mixpanel);

                        }
                        break;
                    }
                }

                return true;
            }
        });

        return convertView;

    }

}