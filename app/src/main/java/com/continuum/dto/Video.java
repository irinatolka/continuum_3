package com.continuum.dto;

import java.util.HashMap;

/**
 * Created by waqas.memon on 2/1/2016.
 */
public class Video {

    String uri = null;
    String name = null;
    String description = null;
    String link = null;
    Double duration = null;
    Picture picture = null;
    String creditsUri = null;
    HashMap<String, String> credits = null;

    public Video(String uri, String name, String description, String link, Double duration) {
        this.uri = uri;
        this.name = name;
        this.description = description;
        this.link = link;
        this.duration = duration;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Double getDuration() {
        return duration;
    }

    public void setDuration(Double duration) {
        this.duration = duration;
    }

    public Picture getPicture() {
        return picture;
    }

    public void setPicture(Picture picture) {
        this.picture = picture;
    }

    public String getCreditsUri() {
        return creditsUri;
    }

    public void setCreditsUri(String creditsUri) {
        this.creditsUri = creditsUri;
    }

    public HashMap<String, String> getCredits() {
        return credits;
    }

    public void setCredits(HashMap<String, String> credits) {
        this.credits = credits;
    }
}