package com.continuum.dto;

import android.graphics.Bitmap;

/**
 * Created by waqas.memon on 2/5/2016.
 */
public class Picture {

    String pictureLink;
    String pictureWidth;
    String pictureHeight;
    Bitmap pictureBitmap;

    public Picture(String pictureLink, String pictureWidth, String pictureHeight, Bitmap pictureBitmap) {
        this.pictureLink = pictureLink;
        this.pictureWidth = pictureWidth;
        this.pictureHeight = pictureHeight;
        this.pictureBitmap = pictureBitmap;
    }

    public Picture(String pictureLink, String pictureWidth, String pictureHeight) {
        this.pictureLink = pictureLink;
        this.pictureWidth = pictureWidth;
        this.pictureHeight = pictureHeight;
    }

    public String getPictureLink() {
        return pictureLink;
    }

    public void setPictureLink(String pictureLink) {
        this.pictureLink = pictureLink;
    }

    public String getPictureWidth() {
        return pictureWidth;
    }

    public void setPictureWidth(String pictureWidth) {
        this.pictureWidth = pictureWidth;
    }

    public String getPictureHeight() {
        return pictureHeight;
    }

    public void setPictureHeight(String pictureHeight) {
        this.pictureHeight = pictureHeight;
    }

    public Bitmap getPictureBitmap() {
        return pictureBitmap;
    }

    public void setPictureBitmap(Bitmap pictureBitmap) {
        this.pictureBitmap = pictureBitmap;
    }
}
