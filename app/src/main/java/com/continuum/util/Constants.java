package com.continuum.util;

/**
 * Created by waqas.memon on 2/20/2016.
 */
public class Constants {
    public static final String SHARED_PREF_TITLE = "CONTINUUM";
    public static final String SHARED_PREF_FLAG = "FIRST_TIME_V9";

    /* VIMEO API */
//    public static final String VIDEO_CHANNEL_URL = "https://api.vimeo.com/me/videos";
//    public static final String AUTH_HEADER = "bearer e9b3f8db4de5e7693d2abf20978559af";
    public static final String BASE_URL = "https://api.vimeo.com";
    public static final String VIDEO_CHANNEL_URL = BASE_URL+"/channels/1030991/videos?fields=status,link,name,description,duration,pictures.sizes,files.quality,files.link_secure,files.link,files.quality.size,metadata&sort=added&direction=desc";
    public static final String AUTH_HEADER = "bearer 80c20c772c75f098b563365692375b0f";
    public static final String CONTENT_TYPE = "application/json";
    public static final int CONNECTION_TIMEOUT = 3 * 1000;
    public static final int WAIT_TIMEOUT = 30 * 1000;
    /* VIMEO API */


    /* MIX PANEL */
    public static final String MIXPANEL_PROJECT_TOKEN = "87b1e1812f38df739ef2fb3b309fbe0c";
    public static final String MIXPANEL_API_KEY = "37cef64f2ce3ee4bc44ed9fcf4200f57";
    public static final String MIXPANEL_API_SECRET = "26365566faa908b1f4aefbacf71b8249";
    public static final String MIXPANEL_EVENT_OPEN = "OPEN";
    public static final String MIXPANEL_EVENT_VIEW_VIDEO_COVER = "VIEW_VIDEO_COVER";
    public static final String MIXPANEL_EVENT_PLAY_VIDEO = "PLAY_VIDEO";
    public static final String MIXPANEL_EVENT_APP_INSTALL = "APP_INSTALL";
    public static final String MIXPANEL_EVENT_PLAY_VIDEO_FULL = "PLAY_VIDEO_COMPLETE";
    public static final String MIXPANEL_EVENT_VIEW_PUBLICATIONS = "VIEW_PUBLICATIONS";
    public static final String MIXPANEL_EVENT_ERROR = "ERROR";
    /* MIX PANEL */


    public static final String PUBLICATIONS_URL = "http://makemecontinuum.com/publication/";

    public static final String VIDEO_QUALITY_MOBILE = "mobile";
    public static final String VIDEO_QUALITY_HLS = "hls";
    public static final String VIDEO_QUALITY_SD = "sd";
    public static final String VIDEO_QUALITY_HD = "hd";



    public static final String EMAIL_ADDRESS_TO = "submissions@makemecontinuum.com";
    public static final String EMAIL_ADDRESS_CC = "waqas.memon@makemecontinuum.com";
    public static final String EMAIL_ADDRESS_BCC = "carl@makemecontinuum.com; Sean@makemecontinuum.com";

    public static final String EMAIL_SUBJECT = "I have a great idea to share with the world...";
    public static final String EMAIL_BODY = "Please upload your video to the service of your choice (YouTube, Vimeo, etc.) and provide us the following information.\n\n" +
            "Your Name: \n"+
            "Video Title: \n" +
            "Video Description: \n" +
            "Video Link: \n\n" +
            "If your video is private, please provide a password to access it.";


    public final static String VIDEO_FEEDS_TAG = "VideoFeedsActivity";
    public final static String VIDEO_FEEDS_DEBUG_TAG = "Continuum - VFA";

    public final static String EXIT_MESSAGE = "Press back button again to exit.";

    public static final int EMAIL_SENDER_ACTIVITY_REQ_CODE=1;
    public static final int PUBLICATIONS_ACTIVITY_REQ_CODE=2;
    public static final int VIDEOPLAYER_ACTIVITY_REQ_CODE=3;



    public static final String WELCOME_SCREEN_1_TITLE = "A CATALYST FOR CHANGE";
    public static final String WELCOME_SCREEN_2_TITLE = "TELL YOUR STORY";
    public static final String WELCOME_SCREEN_3_TITLE = "CREATED BY A NEW GENERATION";
    public static final String WELCOME_SCREEN_4_TITLE = "THE WORLD AWAITS";
    public static final String WELCOME_SCREEN_5_TITLE = "";

    public static final String WELCOME_SCREEN_1_DETAILS = "By propagating great ideas and facilitating meaningful social and professional connections, Continuum elevates both viewers and members. There is a continuum that joins all of us together; from dreamers to doers, artists to inventors, business leaders to community advocates. It’s time to cut through the noise, and collaborate.";
    public static final String WELCOME_SCREEN_2_DETAILS = "With no shortage of talent and great ideas out there to inspire us, there has never been a better time to dream big. But whether we’re looking to learn or to lead, the biggest challenge most of us face is content clutter.";
    public static final String WELCOME_SCREEN_3_DETAILS = "Viewers can enjoy carefully selected video content from real people, streaming to their TV or mobile device, for free. Continuum members can further fuel their passion by connecting with each other. In the era of social media, why settle for a one-sided broadcast?";
    public static final String WELCOME_SCREEN_4_DETAILS = "Continuum is a next-generation video sharing app, available on smart TVs and mobile devices, that brings inspirational content from thought leaders, artists, entrepreneurs, and innovators directly to the people who want to view it.";
    public static final String WELCOME_SCREEN_5_DETAILS = "";

}
