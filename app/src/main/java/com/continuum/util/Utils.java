package com.continuum.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Surface;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.continuum.activity.PublicationsFragment;
import com.continuum.activity.R;
import com.continuum.activity.VideoFeedsActivity;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.readystatesoftware.systembartint.SystemBarTintManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by waqas.memon on 2/13/2016.
 */
public class Utils{
    public static boolean firstTimeLoadFlag = false;
    public static void setStatusBarTranslucent(boolean makeTranslucent, Activity context) {
        SystemBarTintManager tintManager = new SystemBarTintManager(context);
        if (makeTranslucent) {
            tintManager.setStatusBarTintEnabled(true);
            tintManager.setNavigationBarTintEnabled(true);
            tintManager.setTintColor(Color.parseColor("#10000000"));
        } else {
            tintManager.setStatusBarTintEnabled(false);
            tintManager.setNavigationBarTintEnabled(false);
        }


        context.getWindow().getDecorView()
                .setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);

        setStatusBarColor(context);
    }

    private static void setStatusBarColor(Activity context){
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            context.getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
    }

    public static int getStatusBarHeight(Activity activity) {
        Rect r = new Rect();
        Window w = activity.getWindow();
        w.getDecorView().getWindowVisibleDisplayFrame(r);
        return r.top;
    }

    public static int getTitleBarHeight(Activity activity) {
        int viewTop = activity.getWindow().findViewById(Window.ID_ANDROID_CONTENT).getTop();
        return (viewTop - getStatusBarHeight(activity));
    }

    public static void openEmailClient(Activity context){
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("plain/text");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{Constants.EMAIL_ADDRESS_TO});
/*
        intent.putExtra(Intent.EXTRA_CC, new String[] { Constants.EMAIL_ADDRESS_CC });
        intent.putExtra(Intent.EXTRA_BCC, new String[] { Constants.EMAIL_ADDRESS_BCC });
*/
        intent.putExtra(Intent.EXTRA_SUBJECT, Constants.EMAIL_SUBJECT);
        intent.putExtra(Intent.EXTRA_TEXT, Constants.EMAIL_BODY);
        try {
            context.startActivityForResult(Intent.createChooser(intent, "Continuum"), Constants.EMAIL_SENDER_ACTIVITY_REQ_CODE);
        }catch (Exception ex){
            Toast.makeText(context, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }

    public static void openPublicationsFragment(VideoFeedsActivity context){
        try {
            FragmentManager fm = context.getSupportFragmentManager();
            fm.popBackStackImmediate();

            FragmentTransaction ft = fm.beginTransaction();

            PublicationsFragment publicationsFragment = new PublicationsFragment();
            publicationsFragment.init(Constants.PUBLICATIONS_URL);
            ft.replace(R.id.fragment_placeholder, publicationsFragment);
            ft.addToBackStack("PUBLICATIONS");
            ft.commit();

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void highlightBG(ImageView selectedIcon, ImageView... iconsToUnselect){
        selectedIcon.setBackgroundResource(R.drawable.active_btn_light);
        if(iconsToUnselect!=null && iconsToUnselect.length>0) {
            for (ImageView unSelectedIcon : iconsToUnselect) {
                unSelectedIcon.setBackgroundResource(0);
            }
        }
    }
    public static void displayHighlightedBG(ImageView selectedIcon, ImageView... iconsToHide){
        selectedIcon.setVisibility(View.VISIBLE);
        if(iconsToHide!=null && iconsToHide.length>0) {
            for (ImageView unSelectedIcon : iconsToHide) {
                unSelectedIcon.setVisibility(View.INVISIBLE);
            }
        }
    }

    public static void trackMixPanelEvent(MixpanelAPI mixpanel, String event, HashMap<String, String> moreProps){
        try {
            JSONObject props = new JSONObject();
            props.put(event, true);
            if(moreProps!=null && moreProps.size()>0) {
                for (String key : moreProps.keySet()) {
                    props.put(key, moreProps.get(key));
                }
            }
            

            mixpanel.track(event, props);
            mixpanel.flush();
        } catch (JSONException e) {
            Log.e("VideoFeedsActivity", "MIX PANEL Tracking Failed: "+event, e);
            e.printStackTrace();
        }
    }

    public static void flushMixPanel(MixpanelAPI mixpanel){
        Log.i("MixPanel", "flushMixPanel: disabled");
    }


    @NonNull
    public static HashMap<String, String> getErrorStackTraceHashMap(Exception e) {
        HashMap<String, String> moreProps = new HashMap<>();
        moreProps.put("ERROR_MESSAGE", e.getMessage());
        if(e.getCause()!=null)
            moreProps.put("ERROR_CAUSE", e.getCause().getLocalizedMessage());

        if(e.getStackTrace()!=null) {
            int index = 1;
            for (StackTraceElement elem : e.getStackTrace()) {
                moreProps.put("STACK_TRACE "+index, elem.getClassName()+"-->"+elem.getMethodName()+"-->"+elem.getLineNumber());
            }
        }
        return moreProps;
    }


    public static void lockRotation(WindowManager wm, Activity activity){
        if (wm.getDefaultDisplay().getRotation()== Surface.ROTATION_0)
            activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        if (wm.getDefaultDisplay().getRotation()== Surface.ROTATION_90)
            activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        if (wm.getDefaultDisplay().getRotation()== Surface.ROTATION_270)
            activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE);
    }

    public static void unlockRotation(Activity activity){
        activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
    }
}
