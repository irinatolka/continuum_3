package com.continuum.util;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.continuum.activity.R;
import com.continuum.activity.VideoFeedsActivity;

/**
 * Created by waqas.memon on 3/3/2016.
 */

public class WindowUtil {

    @SuppressLint("NewApi")
    public static int getSoftbuttonsbarHeight(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        // getRealMetrics is only available with API 17 and +
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            DisplayMetrics metrics = new DisplayMetrics();
            wm.getDefaultDisplay().getMetrics(metrics);
            int usableHeight = metrics.heightPixels;
            wm.getDefaultDisplay().getRealMetrics(metrics);
            int realHeight = metrics.heightPixels;
            if (realHeight > usableHeight)
                return realHeight - usableHeight;
            else
                return 0;
        }
        return 0;
    }

    @SuppressLint("NewApi")
    public static int getSoftbuttonsbarWidth(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        // getRealMetrics is only available with API 17 and +
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            DisplayMetrics metrics = new DisplayMetrics();
            wm.getDefaultDisplay().getMetrics(metrics);
            int usableWidth = metrics.widthPixels;
            wm.getDefaultDisplay().getRealMetrics(metrics);
            int realWidth = metrics.widthPixels;
            if (realWidth> usableWidth)
                return realWidth - usableWidth;
            else
                return 0;
        }
        return 0;
    }

    @SuppressLint("NewApi")
    public static int getRealScreenHeight(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        // getRealMetrics is only available with API 17 and +
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            DisplayMetrics metrics = new DisplayMetrics();
/*
            wm.getDefaultDisplay().getMetrics(metrics);
            int usableHeight = metrics.heightPixels;
*/
            wm.getDefaultDisplay().getRealMetrics(metrics);
            int realHeight = metrics.heightPixels;
//            if (realHeight > usableHeight)
                return realHeight; //- usableHeight;
//            else
//                return 0;
        }
        return 0;
    }

    @SuppressLint("NewApi")
    public static int getRealScreenWidth(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        // getRealMetrics is only available with API 17 and +
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            DisplayMetrics metrics = new DisplayMetrics();
//            wm.getDefaultDisplay().getMetrics(metrics);
//            int usableWidth = metrics.widthPixels;
            wm.getDefaultDisplay().getRealMetrics(metrics);
            int realWidth = metrics.widthPixels;
//            if (realWidth> usableWidth)
                return realWidth; //- usableWidth;
//            else
//                return 0;
        }
        return 0;
    }

    public static Point getScreenDimensions(Context context){
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size;
    }


    private int mSystemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION;

    /**
     * Hides StatusBar and ActionBar
     */
    public static void hideSystemUi(AppCompatActivity appCompatActivity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            appCompatActivity.getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                            | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            appCompatActivity.getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
    }

    /**
     * Shows StatusBar and ActionBar
     */
    public static void showSystemUi(AppCompatActivity appCompatActivity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            appCompatActivity.getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
    }

    public static void enablePreventScreenLock(AppCompatActivity appCompatActivity) {
        appCompatActivity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    public static void disablePreventScreenLock(AppCompatActivity appCompatActivity) {
        appCompatActivity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    public static Point getFooterLayoutDimensions(Context context){
        VideoFeedsActivity activity = (VideoFeedsActivity) context;
        final View footerLayout = activity.findViewById(R.id.footerLayout);

        Point size = new Point();

        if(footerLayout!=null){
            size.set(footerLayout.getWidth(), footerLayout.getHeight());
        }else{
            size.set(0,0);
        }

        return size;

    }


    public static Point getVideoFeedsLayoutDimensions(Context context){
        VideoFeedsActivity activity = (VideoFeedsActivity) context;
        final View layout = activity.findViewById(R.id.videoFeedsLayout);

        Point size = new Point();

        if(layout!=null){
            size.set(layout.getWidth(), layout.getHeight());
        }else{
            size.set(0, 0);
        }

        return size;

    }

    public static void setLayoutHeight(View view, int y){
        ViewGroup.LayoutParams params = view.getLayoutParams();
        params.height = y;
    }

    public static void setLayoutWidth(View view, int x){
        ViewGroup.LayoutParams params = view.getLayoutParams();
        params.width = x;


    }
}
